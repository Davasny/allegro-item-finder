"""Allegro finder

Usage:
  app.py search -i <items> [--category <category>] [--smart] [--max-pages <pages>] [--optimal-common-sellers-count <number>] [--sort <sorting_type>]
  app.py crawl -i <items> [--category <category>] [--smart] [--max-pages <pages>] [--sort <sorting_type>]

Options:
  -i <items>                            List of items splitted by comma (,)
  -c --category <category>              Category, should be a string from allegro site like "gps-i-akcesoria-67093"
  -s --smart                            Look only for offers with Allegro SMART
  -m --max-pages <pages>                Maximum pages to iterate [default: 10]
  -o --optimal-common-sellers-count <number>   Count of sellers with all your items that makes you happy [default: 4]
  -r --sort <sorting_type>              Allegro sorting type (popularity - popularity growing, price-price growing,
                                        priced - price descending, ps-price with shipment growing,
                                        psd - price with shipment descending, time - time to end, new - newest first,
                                        accuracy) [default: accuracy]
  -h --help                             Show this screen.
  --version                             Show version.
"""
from allegro_connector import AllegroConnector
import config
from docopt import docopt
from os import path, mkdir
from terminaltables import AsciiTable
import datetime
import json

if __name__ == '__main__':
    arguments = docopt(__doc__, version='Allegro finder 1.0')

    allegro = AllegroConnector()

    items = arguments['-i'].split(',')
    items_string = '", "'.join(items)

    all_offers = {}
    sellers_by_item = {}

    table_data = [['Seller', f'Item (for "{items_string}")', 'Price', 'Link', 'Bought by']]

    for i in range(1, int(arguments['--max-pages'])+1):
        sellers_ids = []
        for item in items:
            item_page = allegro.search_for_item(item, page_num=i, category=arguments['--category'],
                                                smart=arguments['--smart'], sort=arguments['--sort'])

            if not item_page[0]:
                raise Exception(item_page[1])  # something went wrong in method

            offers_by_seller_id = allegro.get_detailed_offers(item_page[1])  # all offers with details by seller id
            for seller_id in offers_by_seller_id:
                if seller_id not in all_offers:
                    all_offers[seller_id] = []

                for offer in offers_by_seller_id[seller_id]:
                    all_offers[seller_id].append(offer)

            if item not in sellers_by_item:
                sellers_by_item[item] = []

            sellers_by_item[item] += set(offers_by_seller_id)
            sellers_ids.append(sellers_by_item[item])

        if arguments['search']:
            common_sellers = set.intersection(*map(set, sellers_ids))
            print(f'Found so far {len(common_sellers)}/{arguments["--optimal-common-sellers-count"]} sellers at {i} pages')

            if len(common_sellers) >= int(arguments['--optimal-common-sellers-count']):
                break

    if arguments['search']:
        for seller_id in common_sellers:
            for offer in all_offers[seller_id]:
                table_data.append([
                    all_offers[seller_id][0]['seller']['userListingUrl'],
                    offer['title']['text'],
                    f"{offer['price']['normal']['amount']}zł",
                    offer['url'],
                    offer['boughtBy']
                ])
            table_data.append([])

        table_instance = AsciiTable(table_data)
        table_instance.justify_columns = {2: 'center'}
        print(table_instance.table)

    elif arguments['crawl']:
        if not path.exists(config.CRAWLED_DIR):
            mkdir(config.CRAWLED_DIR)

        today = datetime.datetime.today()
        file_name = f'{config.CRAWLED_DIR[:-1]}/{today.strftime("%Y-%m-%d %H-%M-%S")}.json'
        with open(file_name, 'w', encoding='utf8') as file:
            file.write(json.dumps(all_offers, ensure_ascii=False))
        print(f'File created: {file_name} with {len(all_offers)} offers')
