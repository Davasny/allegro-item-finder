import requests
import config
import re
import json
from os import path


class AllegroConnector:
    def __init__(self):
        self._s = requests.Session()
        self._categories = {}  # id: name
        if path.exists(config.CATEGORIES_FILE):
            with open(config.CATEGORIES_FILE, 'r', encoding='utf8') as file:
                self._categories = json.loads(file.read())

    def __del__(self):
        with open(config.CATEGORIES_FILE, 'w', encoding='utf8') as file:
            file.write(json.dumps(self._categories, ensure_ascii=False, indent=2))

    def search_for_item(self, item_name, page_num=1, category=None, smart=False, sort=None):
        """
        Search for item on allegro in /listing (default web search)

        :param item_name: Name of item that you are looking for
        :param page_num: Number of page with search results
        :param category: Category, should be a string from allegro site like "gps-i-akcesoria-67093"
        :param smart: Look for offers only from SMART program sellers
        :param sort: Add sort argument to allegro URL
        :return: <bool: result>, <str: page source>
        """

        category_string = ''
        if category is not None:
            category_string = f'/kategoria/{category}'

        _sort_map = {
            'popularity': 'qd',
            'priced': 'pd',
            'price': 'p',
            'ps': 'd',
            'psd': 'dd',
            'time': 't',
            'new': 'n',
            'accuracy': ''
        }

        sort_string = ''
        if sort is not None:
            sort_string = _sort_map[sort]

        params = {
            'string': item_name,
            'p': str(page_num),
            'allegro-smart-standard': '1' if smart else '0',
            'order': sort_string
        }

        url = f'{config.ALLEGRO_BASE_URL}{category_string}/listing'
        response = self._s.get(url, params=params)

        if response.status_code >= 400:
            return False, {'url': url, 'status_code': response.status_code, 'text': response.text}
        return True, response.text

    def get_detailed_offers(self, page_source):
        """
        Parse source code of search result with xpath's and return dictionary with sellers of items

        :param page_source: <str>
        :return: { "seller id": [ {offer}, ... ]}
        """
        offers = json.loads((re.findall("^.*__listing_StoreState_base.*$", page_source, re.MULTILINE)[0]
                             .split("'__listing_StoreState_base'] = ")[1]
                             .replace(';</script>', '')))

        offers_by_seller_id = {}

        group_num = 0
        last_len = 0
        i = 0
        # get largest list from itemsGroups
        for group in offers['items']['itemsGroups']:
            if len(group['items']) > last_len:
                group_num = i
                last_len = len(group['items'])
            i += 1

        for offer in offers['items']['itemsGroups'][group_num]['items']:
            if "allegrolokalnie" not in offer['url']:
                offer['seller']['id'] = int(offer['seller']['id'])
                offer['id'] = int(offer['id'])
                seller_id = offer['seller']['id']

                if seller_id not in offers_by_seller_id:
                    offers_by_seller_id[seller_id] = []

                offers_by_seller_id[seller_id].append(offer)

                offer['categories'] = []
                for cat in offer['categoryPath']:
                    if cat not in self._categories:
                        self._get_categories_names(offer['url'])
                    offer['categories'].append(self._categories[cat])

                bought_by = re.search(r'\d+', offer['bidInfo'])
                if bought_by is not None:
                    bought_by = int(bought_by[0])
                else:
                    bought_by = 0

                offer['price']['normal']['amount'] = float(offer['price']['normal']['amount'])
                offer['boughtBy'] = bought_by
        return offers_by_seller_id

    def _get_categories_names(self, url):
        response = self._s.get(url)
        if response.status_code < 400:
            matched_regex = re.findall(r'( opbox.config = opbox.config .*$)', response.text, re.MULTILINE)
            res = json.loads((matched_regex[0])
                             .split(' opbox.config = opbox.config || ')[1]
                             .replace(' || {};</script>', ''))
            for cat in res['cookieMonster']['defaultCustomParams']['pv']['categoryPath']:
                if re.search(r'[a-z]', cat['id']) is None:
                    # not UID
                    self._categories[cat['id']] = cat['name']