# allegro-item-finder

Program allows you to find multiple items on allegro from one seller

## Usage

### From source
1. Install requirements
```shell
pip install -r app/requirements.txt
```

2. Run `app.py`
```shell
Usage:
  app.py search -i <items> [--category <category>] [--smart] [--max-pages <pages>] [--optimal-common-sellers-count <number>]

Options:
  -i <items>                            List of items splitted by comma (,)
  -c --category <category>              Category, should be a string from allegro site like "gps-i-akcesoria-67093"
  -s --smart                            Look only for offers with Allegro SMART
  -m --max-pages <pages>                Maximum pages to iterate [default: 10]
  -o --optimal-common-sellers-count <number>   Count of sellers with all your items that makes you happy [default: 4]
  -h --help                             Show this screen.
  --version                             Show version.
```

### Using docker
```
docker run -it registry.gitlab.com/davasny/allegro-item-finder --help
```

## Sample output
```
> docker run -it allegro-finder search -i "gps sma,u.fl"

Found so far 0/4 sellers at 1 pages
Found so far 1/4 sellers at 2 pages
Found so far 3/4 sellers at 3 pages
Found so far 4/4 sellers at 4 pages
Found so far 5/4 sellers at 5 pages
+-----------------------------------------------+----------------------------------------------------+----------+----------------------------------------------------------------------------------------+
| Seller                                        | Item (for "gps sma", "u.fl")                       |  Price   | Link                                                                                   |
+-----------------------------------------------+----------------------------------------------------+----------+----------------------------------------------------------------------------------------+
| https://allegro.pl/uzytkownik/czar_82         | Adapter U.FL (IPEX) - SMA żeńskie 20cm             |  5.50zł  | https://allegro.pl/oferta/adapter-u-fl-ipex-sma-zenskie-20cm-7977937155                |
| https://allegro.pl/uzytkownik/czar_82         | Adapter U.FL (IPEX) - SMA żeńskie 10cm             |  4.70zł  | https://allegro.pl/oferta/adapter-u-fl-ipex-sma-zenskie-10cm-7977934986                |
| https://allegro.pl/uzytkownik/czar_82         | Adapter U.FL (IPEX) - SMA męskie 10cm              |  7.50zł  | https://allegro.pl/oferta/adapter-u-fl-ipex-sma-meskie-10cm-7977922858                 |
| https://allegro.pl/uzytkownik/czar_82         | Antena GPS 1575.42MHz SMA 3-5V magnes              | 25.00zł  | https://allegro.pl/oferta/antena-gps-1575-42mhz-sma-3-5v-magnes-7977981595             |
|                                               |                                                    |          |                                                                                        |
| https://allegro.pl/uzytkownik/atelektronics   | Antena wewnętrzna GPS SMA-C 4CARMEDIA kątowy 5m    | 42.00zł  | https://allegro.pl/oferta/antena-wewnetrzna-gps-sma-c-4carmedia-katowy-5m-7033344824   |
| https://allegro.pl/uzytkownik/atelektronics   | Antena GPS 1575.42MHz SMA 3-5V na magnes           | 39.00zł  | https://allegro.pl/oferta/antena-gps-1575-42mhz-sma-3-5v-na-magnes-7374896338          |
| https://allegro.pl/uzytkownik/atelektronics   | LED S_5U75_55_FL_L8_REV1.5_150514 LM41-00135A      | 30.00zł  | https://allegro.pl/oferta/led-s-5u75-55-fl-l8-rev1-5-150514-lm41-00135a-8153574781     |
| https://allegro.pl/uzytkownik/atelektronics   | LED S_5U75_55_FL_R6_REV1.5_150514 LM41-00136A      | 30.00zł  | https://allegro.pl/oferta/led-s-5u75-55-fl-r6-rev1-5-150514-lm41-00136a-8153580330     |
|                                               |                                                    |          |                                                                                        |
| https://allegro.pl/uzytkownik/botland_pl      | SparkFun moduł GPS ZOE-M8Q - Qwiic - antena U.FL   | 209.00zł | https://allegro.pl/oferta/sparkfun-modul-gps-zoe-m8q-qwiic-antena-u-fl-8266197646      |
| https://allegro.pl/uzytkownik/botland_pl      | Moduł XBee 802.15.4 + BLE Seria 3 - antena U.FL    | 125.00zł | https://allegro.pl/oferta/modul-xbee-802-15-4-ble-seria-3-antena-u-fl-8287808180       |
| https://allegro.pl/uzytkownik/botland_pl      | Antena GPS ze złączem SMA mocowana magnetycznie    | 43.90zł  | https://allegro.pl/oferta/antena-gps-ze-zlaczem-sma-mocowana-magnetycznie-8251091842   |
| https://allegro.pl/uzytkownik/botland_pl      | Antena WiFi 5dB + przejściówka SMA - U.FL          |  8.50zł  | https://allegro.pl/oferta/antena-wifi-5db-przejsciowka-sma-u-fl-8226026117             |
| https://allegro.pl/uzytkownik/botland_pl      | Antena WiFi 3dB U.FL                               |  6.75zł  | https://allegro.pl/oferta/antena-wifi-3db-u-fl-8226026586                              |
| https://allegro.pl/uzytkownik/botland_pl      | Antena WiFi 6dBi U.FL do LattePanda                | 19.95zł  | https://allegro.pl/oferta/antena-wifi-6dbi-u-fl-do-lattepanda-8232164805               |
| https://allegro.pl/uzytkownik/botland_pl      | Antena WiFi 2dB U.FL - do modułów SiPy i LoPy      | 77.00zł  | https://allegro.pl/oferta/antena-wifi-2db-u-fl-do-modulow-sipy-i-lopy-8232547082       |
|                                               |                                                    |          |                                                                                        |
| https://allegro.pl/uzytkownik/ANKA1963        | Antena WIFI 2.4GHz U.FL 25cm                       |  6.99zł  | https://allegro.pl/oferta/antena-wifi-2-4ghz-u-fl-25cm-7387708755                      |
| https://allegro.pl/uzytkownik/ANKA1963        | Adapter U.FL (IPEX) - SMA męskie 10cm              |  8.99zł  | https://allegro.pl/oferta/adapter-u-fl-ipex-sma-meskie-10cm-7311762008                 |
| https://allegro.pl/uzytkownik/ANKA1963        | Antena GPS 1575.42MHz SMA 3-5V magnes              | 24.90zł  | https://allegro.pl/oferta/antena-gps-1575-42mhz-sma-3-5v-magnes-7328650974             |
|                                               |                                                    |          |                                                                                        |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód u.FL (f) - FME (m) panel 200 mm kabel 1.37 | 22.90zł  | https://allegro.pl/oferta/przewod-u-fl-f-fme-m-panel-200-mm-kabel-1-37-7736681615      |
| https://allegro.pl/uzytkownik/balticad_gdansk | Antena wewnętrzna WiFi 2400 5800MHz 2dBi u FL IPX  | 17.90zł  | https://allegro.pl/oferta/antena-wewnetrzna-wifi-2400-5800mhz-2dbi-u-fl-ipx-7502553649 |
| https://allegro.pl/uzytkownik/balticad_gdansk | Antena LTE wewnętrzna 67x58x0.2 mm 15cm u.FL / IPX | 19.90zł  | https://allegro.pl/oferta/antena-lte-wewnetrzna-67x58x0-2-mm-15cm-u-fl-ipx-7224496283  |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód IPX u.FL (f) - SMA (f) 20 cm kabel RG178   | 17.90zł  | https://allegro.pl/oferta/przewod-ipx-u-fl-f-sma-f-20-cm-kabel-rg178-7476972125        |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód u.FL (f) IPX - SMA (f) 13 cm kabel 1.13    | 18.90zł  | https://allegro.pl/oferta/przewod-u-fl-f-ipx-sma-f-13-cm-kabel-1-13-7309996979         |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód kabel IPX u.FL (f) - u.FL (f) 102mm 1.13   |  9.90zł  | https://allegro.pl/oferta/przewod-kabel-ipx-u-fl-f-u-fl-f-102mm-1-13-7309997989        |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód kabel IPX u.FL (f) - SMA (f) 1.13mm 80mm   | 17.50zł  | https://allegro.pl/oferta/przewod-kabel-ipx-u-fl-f-sma-f-1-13mm-80mm-7309998245        |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód IPX u.FL (f) - u.FL (f) 20cm kabel 0.81    | 22.50zł  | https://allegro.pl/oferta/przewod-ipx-u-fl-f-u-fl-f-20cm-kabel-0-81-7309998378         |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód Pigtail u.FL IPX - SMA-RP 20cm kabel 1.13  | 18.90zł  | https://allegro.pl/oferta/przewod-pigtail-u-fl-ipx-sma-rp-20cm-kabel-1-13-7309995629   |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód IPX u.FL (f) - FME (m) 10 cm kabel 1.13    | 34.90zł  | https://allegro.pl/oferta/przewod-ipx-u-fl-f-fme-m-10-cm-kabel-1-13-7309998764         |
| https://allegro.pl/uzytkownik/balticad_gdansk | Pigtail kabel IPX u.FL (f) - SMA (f) 1.13mm 68mm   | 17.50zł  | https://allegro.pl/oferta/pigtail-kabel-ipx-u-fl-f-sma-f-1-13mm-68mm-7309998109        |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód IPX u.FL (f) - u.FL (f) 3.5cm kabel 0.81   |  9.90zł  | https://allegro.pl/oferta/przewod-ipx-u-fl-f-u-fl-f-3-5cm-kabel-0-81-7309997388        |
| https://allegro.pl/uzytkownik/balticad_gdansk | Antena GSM UMTS wewnętrzna u FL IPX 10cm 37x7x1mm  | 15.90zł  | https://allegro.pl/oferta/antena-gsm-umts-wewnetrzna-u-fl-ipx-10cm-37x7x1mm-7426937997 |
| https://allegro.pl/uzytkownik/balticad_gdansk | Antena GSM PCB 5-zakresowa u.FL 20cm 40x15x0.7mm   | 19.90zł  | https://allegro.pl/oferta/antena-gsm-pcb-5-zakresowa-u-fl-20cm-40x15x0-7mm-7674743334  |
| https://allegro.pl/uzytkownik/balticad_gdansk | Antena GSM wewnętrzna, 2dBi, 10cm u.FL, 34x7mm     | 15.90zł  | https://allegro.pl/oferta/antena-gsm-wewnetrzna-2dbi-10cm-u-fl-34x7mm-7801621460       |
| https://allegro.pl/uzytkownik/balticad_gdansk | Antena GSM wewnętrzna 2dBi u FL 10cm 47x7.6x1.2mm  | 23.90zł  | https://allegro.pl/oferta/antena-gsm-wewnetrzna-2dbi-u-fl-10cm-47x7-6x1-2mm-7426938467 |
| https://allegro.pl/uzytkownik/balticad_gdansk | Antena GSM wewnętrzna 2dBi, 10cm u.FL; 36x13,2mm   | 19.90zł  | https://allegro.pl/oferta/antena-gsm-wewnetrzna-2dbi-10cm-u-fl-36x13-2mm-7801621798    |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód u.FL (f) - SMA (f) panel 30 cm kabel 1.13  | 10.90zł  | https://allegro.pl/oferta/przewod-u-fl-f-sma-f-panel-30-cm-kabel-1-13-7428922486       |
| https://allegro.pl/uzytkownik/balticad_gdansk | Złącze u.FL IPX (m) PCB 3 pady SMD                 |  4.90zł  | https://allegro.pl/oferta/zlacze-u-fl-ipx-m-pcb-3-pady-smd-7514464462                  |
| https://allegro.pl/uzytkownik/balticad_gdansk | Kabel przewód Pigtail u.FL (f) SMA (f) 1.13mm 80mm | 17.50zł  | https://allegro.pl/oferta/kabel-przewod-pigtail-u-fl-f-sma-f-1-13mm-80mm-7098918058    |
| https://allegro.pl/uzytkownik/balticad_gdansk | Pigtail kabel u.FL IPX kątowy - SMA (f) panel 52mm | 17.50zł  | https://allegro.pl/oferta/pigtail-kabel-u-fl-ipx-katowy-sma-f-panel-52mm-7402431577    |
| https://allegro.pl/uzytkownik/balticad_gdansk | Pigtail kabel u.FL (f) kątowy SMA (f) 1.13mm 68mm  | 17.50zł  | https://allegro.pl/oferta/pigtail-kabel-u-fl-f-katowy-sma-f-1-13mm-68mm-7260125145     |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód u.FL (f) - u.FL (f) 20cm kabel 0.81 White  | 10.90zł  | https://allegro.pl/oferta/przewod-u-fl-f-u-fl-f-20cm-kabel-0-81-white-7224497801       |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód kabel u FL (f) - u.FL (f) 102mm 1.13       |  9.90zł  | https://allegro.pl/oferta/przewod-kabel-u-fl-f-u-fl-f-102mm-1-13-7224497420            |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód u.FL (f) - u.FL (f), 145mm, 1.13           | 10.58zł  | https://allegro.pl/oferta/przewod-u-fl-f-u-fl-f-145mm-1-13-7794916794                  |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód IPX u.FL (f) - FME (m) 10 cm kabel 1.13    | 14.90zł  | https://allegro.pl/oferta/przewod-ipx-u-fl-f-fme-m-10-cm-kabel-1-13-7375244551         |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód u.FL - u FL (IPX) 3.5cm kabel 0.81 biały   |  9.90zł  | https://allegro.pl/oferta/przewod-u-fl-u-fl-ipx-3-5cm-kabel-0-81-bialy-7107111701      |
| https://allegro.pl/uzytkownik/balticad_gdansk | Pigtail u.FL (f) kątowy- FME (m) panel 1.13mm 80mm | 14.82zł  | https://allegro.pl/oferta/pigtail-u-fl-f-katowy-fme-m-panel-1-13mm-80mm-7476675002     |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód u.FL (f) - FME (m) panel 10 cm kabel 1.13  | 18.00zł  | https://allegro.pl/oferta/przewod-u-fl-f-fme-m-panel-10-cm-kabel-1-13-7276264084       |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód u.FL (f) SMA-RP (m) panel 20cm kabel 1.13  | 18.90zł  | https://allegro.pl/oferta/przewod-u-fl-f-sma-rp-m-panel-20cm-kabel-1-13-7188597171     |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód u.FL (f) - SMA (f) 1.13mm 50mm wodoodporny | 21.50zł  | https://allegro.pl/oferta/przewod-u-fl-f-sma-f-1-13mm-50mm-wodoodporny-7875294688      |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód kabel u.FL (f) RA - MCX (m), 20cm 1.13     | 12.92zł  | https://allegro.pl/oferta/przewod-kabel-u-fl-f-ra-mcx-m-20cm-1-13-7402432506           |
| https://allegro.pl/uzytkownik/balticad_gdansk | Przewód u.FL (f) - SMB (m) panel 85mm kabel 1.13   | 20.30zł  | https://allegro.pl/oferta/przewod-u-fl-f-smb-m-panel-85mm-kabel-1-13-7402433044        |
| https://allegro.pl/uzytkownik/balticad_gdansk | Antena combo 3G GPS 28dBi przykręcana 2x 3m SMA    | 79.90zł  | https://allegro.pl/oferta/antena-combo-3g-gps-28dbi-przykrecana-2x-3m-sma-7799225134   |
| https://allegro.pl/uzytkownik/balticad_gdansk | Antena combo 3G GPS 28dBi przykręcana 2x 3m SMA    | 69.90zł  | https://allegro.pl/oferta/antena-combo-3g-gps-28dbi-przykrecana-2x-3m-sma-7801621613   |
|                                               |                                                    |          |                                                                                        |
+-----------------------------------------------+----------------------------------------------------+----------+----------------------------------------------------------------------------------------+

```
